from sqlalchemy import Column, BigInteger, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from .base import Base


class Alert(Base):
	__tablename__ = "alert"

	id = Column(Integer, primary_key=True, autoincrement=True)
	name = Column(String, nullable=False)
	user_id = Column(Integer, ForeignKey("user.id"), nullable=True)
	team_id = Column(Integer, ForeignKey("team.id"), nullable=True)

	user = relationship("User")
	team = relationship("Team")


class AlertNotification(Base):
	__tablename__ = "alert_notification"

	id = Column(BigInteger, primary_key=True, autoincrement=True)
	alert_id = Column(Integer, ForeignKey("alert.id"), nullable=False)
	pair_id = Column(Integer, ForeignKey("pair.id"), primary_key=False)
	type = Column(String, nullable=False)
	data = Column(String, nullable=True)

	alert = relationship("Alert")
	pair = relationship("Pair")


class AlertRule(Base):
	__tablename__ = "alert_rule"

	id = Column(Integer, primary_key=True, autoincrement=True)
	alert_id = Column(Integer, ForeignKey("alert.id"), nullable=False)
	script_id = Column(Integer, ForeignKey("alert_script.id"), nullable=False)
	collation_symbol = Column(String, nullable=False)
	collation_target = Column(String, nullable=False)

	alert = relationship("Alert")
	script = relationship("AlertScript")


class AlertScript(Base):
	__tablename__ = "alert_script"

	id = Column(Integer, primary_key=True, autoincrement=True)
	name = Column(String, nullable=False)


class AlertPair(Base):
	__tablename__ = "alert_pair"

	alert_id = Column(Integer, ForeignKey("alert.id"), primary_key=True)
	pair_id = Column(Integer, ForeignKey("pair.id"), primary_key=True)

	alert = relationship("Alert")
	pair = relationship("Pair")
