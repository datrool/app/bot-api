from types import SimpleNamespace
from datetime import datetime
from sqlalchemy import (
	Column, Integer, BigInteger, String, Boolean, DateTime, Text,
	ForeignKey, UniqueConstraint, CheckConstraint
)
from sqlalchemy.orm import relationship
from sqlalchemy.types import ARRAY
from .base import Base

from pydantic import BaseModel, Field, ValidationError, validator
from typing import Optional


class StrategyValidatorCreate(BaseModel):
	name: str
	public: bool
	user_id: Optional[int] = None
	team_id: Optional[int] = None

	@validator("user_id")
	def team_or_user(cls, v, values):
		if not v and "team_id" not in values:
			raise ValidationError('One of field user_id or team_id must have a valid value')
		if v and v < 0:
			raise ValidationError('Invalid user id')
		return v

	@validator("user_id")
	def team_id_validity(cls, v, values):
		if v and v < 0:
			raise ValidationError('Invalid team id')
		return v


class StrategyValidator(StrategyValidatorCreate):
	id: int = Field(..., gt=0)


class StrategyVersionValidatorCreate(BaseModel):
	strategy_id: int = Field(..., gt=0)
	major: int = Field(..., gt=0)
	minor: int = Field(..., ge=0)
	revision: int = Field(..., ge=0)
	code: str
	created: datetime = datetime.utcnow()
	created_by_id: int = Field(..., ge=0)


class StrategyVersionValidator(StrategyVersionValidatorCreate):
	id: int = Field(..., gt=0)


class Strategy(Base):
	__tablename__ = "strategy"
	__table_args__ = (
		UniqueConstraint("name", "user_id", "team_id", name="uq_strategy_name"),
		CheckConstraint(
			"(user_id IS NULL OR team_id IS NULL) AND (user_id IS NOT NULL OR team_id IS NOT NULL)",
			name="ck_strategy_user_or_team"
		)
	)

	id = Column(BigInteger, primary_key=True, autoincrement=True)
	name = Column(String, nullable=False)
	public = Column(Boolean, nullable=False, default=False)
	user_id = Column(Integer, ForeignKey('user.id'), nullable=True)
	team_id = Column(Integer, ForeignKey('team.id'), nullable=True)

	team = relationship('Team')
	user = relationship('User')
	versions = relationship(
		"StrategyVersion",
		back_populates="strategy",
		order_by=lambda: [
			StrategyVersion.major.desc(),
			StrategyVersion.minor.desc(),
			StrategyVersion.revision.desc()
		]
	)

	@classmethod
	def create(cls, data: StrategyValidatorCreate):
		instance = cls(**{
			"name": data.name,
			"public": data.public
		})
		if data.team_id:
			instance.team_id = data.team_id
		else:
			instance.user_id = data.user_id
		return instance

	def update(self, data: StrategyValidator):
		self.name = data.name
		self.public = data.public

	@property
	def owner(self):
		if self.user_id:
			return SimpleNamespace(type="user", id=self.user_id, name=self.user.nick)
		else:
			return SimpleNamespace(type="team", id=self.team_id, name=self.team.name)


class StrategyVersion(Base):
	__tablename__ = "strategy_version"
	__table_args__ = (
		UniqueConstraint(
			"strategy_id", "major", "minor", "revision",
			name="uq_strategy_version_strategy_id"
		),
	)

	id = Column(BigInteger, primary_key=True, autoincrement=True)
	strategy_id = Column(BigInteger, ForeignKey("strategy.id"), nullable=False)
	major = Column(Integer, nullable=False)
	minor = Column(Integer, nullable=False)
	revision = Column(Integer, nullable=False)
	code = Column(Text, nullable=False)
	requirements = Column(ARRAY(String), nullable=True)
	created = Column(DateTime, nullable=False, default=datetime.utcnow)
	created_by_id = Column(Integer, ForeignKey("user.id"), nullable=False)

	strategy = relationship("Strategy", back_populates="versions")
	created_by = relationship("User")

	@classmethod
	def create(cls, data: StrategyVersionValidatorCreate):
		return cls(**{
			"strategy_id": data.strategy_id,
			"major": data.major,
			"minor": data.minor,
			"revision": data.revision,
			"code": data.code,
			"created": data.created,
			"created_by_id": data.created_by_id
		})

	@property
	def version(self):
		return "%s.%s.%s" % (self.major, self.minor, self.revision)
