from datetime import datetime, timedelta
from re import compile
from random import choice
from string import ascii_letters, digits
from bcrypt import checkpw, hashpw, gensalt
from lib.database import Database as db
from model.user import User, Authentication


class Auth:
	__slots__ = ("_right", "_method")
	__tokens: dict = {}
	__users: dict = {}

	def __init__(self, right=None):
		self._right = right

	def __call__(self, method):
		self._method = method
		return self._wrap

	async def _wrap(self, req, resp, *args, **kwargs):
		try:
			ik = int(req.headers.get("token_ik"))
		except ValueError:
			ik = None
		base = req.headers.get("token")
		device = req.headers.get("token_device")
		team_id = req.headers.get("team_id", 0)

		status, data, user = self.check(ik, base, device, team_id, self._right)
		if status is not True:
			await resp.abort(status=status, data=data)
		return await self._method(req, resp, user, *args, **kwargs)

	@staticmethod
	def check(ik, base, device, team_id=0, right=None):
		if type(ik) != int or not (user := Auth._check(ik, base, device)):
			return 401, {"code": 1001, "message": "Authentication failed"}, None
		try:
			team_id = int(team_id)
		except Exception:
			return 400, {"code": 1000, "message": "Invalid team id"}, None
		if team_id:
			if team_id not in user.team_ids:
				return 401, {"code": 1002, "message": "Not member"}
			elif right and right in user.rights[team_id]:
				return 401, {"code": 1003, "message": "Not granted"}
		return True, False, user

	@staticmethod
	def _check(ik, base, device):
		token = Auth.__tokens.get(ik)
		if not token and (result := db.query(Authentication).get(ik)):
			token = {
				"key": result.key_hash.encode(),
				"device": result.device_hash,
				"user": result.user,
				"expire": result.expiration
			}
		elif not token:
			return None
		if token["device"] == device and checkpw(base.encode(), token["key"]):
			Auth.__tokens[ik] = token
			if token["user"].id not in Auth.__users:
				Auth.__users[result.user_id] = token["user"]
			token["user"].token_count += 1
			token["used"] = datetime.now()
			return token["user"]
		return None

	@staticmethod
	def create_token(username, password, device_name=None):
		pattern = compile(r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')
		if pattern.match(username):
			user = db.query(User).filter_by(email=username).one_or_none()
		else:
			user = db.query(User).filter_by(nick=username).one_or_none()

		if not user or not checkpw(password.encode(), user.password.encode()):
			return False

		key = Auth.generate_key(64)
		while (
			db.query(Authentication)
			.filter(Authentication.key_hash == hashpw(key.encode(), gensalt()).decode())
			.count()
		):
			key = Auth.generate_key(64)
		device_hash = Auth.generate_key(16)
		while (
				db.query(Authentication)
				.filter(Authentication.device_hash == device_hash)
				.count()
		):
			device_hash = User.generate_key(16)
		auth = Authentication(
			user_id=user.id,
			device_hash=device_hash,
			key_hash=hashpw(key.encode(), gensalt()).decode(),
			device_name=device_name,
			expiration=datetime.utcnow() + timedelta(hours=user.token_expiration),
		)
		db.session.add(auth)
		db.session.commit()
		Auth.__tokens[auth.id] = {
			"key": auth.key_hash.encode(),
			"device": auth.device_hash,
			"user": user,
			"used": datetime.now(),
			"expire": auth.expiration
		}
		if not Auth.__users.get(user.id):
			Auth.__users[user.id] = user
		user.token_count += 1
		return auth.id, key, device_hash

	@staticmethod
	def generate_key(length):
		return ''.join(choice(ascii_letters + digits) for i in range(length))

	@staticmethod
	def refresh(user_id):
		token_count = Auth.__users[user_id].token_count
		Auth.__users[user_id] = db.query(User).get(user_id)
		Auth.__users[user_id].token_count = token_count

	@staticmethod
	def clean():
		now = datetime.utcnow()
		db.query(Authentication).filter(now <= Authentication.expire).delete()
		for id, data in Auth.__tokens.items():
			user = Auth.__users[data["user_id"]]
			if now <= data["expire"] or data["used"] < now - timedelta(minutes=5):
				Auth.__tokens.pop(id)
				user.token_count -= 1
			if not user.token_count:
				Auth.__users.pop(data["user_id"])
