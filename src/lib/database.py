from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from functools import wraps


class hybridmethod(object):
	def __init__(self, call):
		self.call = call

	def __get__(self, obj, cls):
		context = obj if obj is not None else cls
		@wraps(self.call)
		def hybrid(*args, **kw):
			return self.call(context, *args, **kw)
		return hybrid


class Database:
	engine = None
	session = None

	def __init__(self, user, password, host, database):
		self.init(user, password, host, database, target=self)

	@staticmethod
	def init(user, password, host, database, target=None):
		target = target or Database
		target.engine = create_engine(
			"postgresql+psycopg2://%s:%s@%s/%s" % (user, password, host, database),
			pool_pre_ping=True,
			pool_recycle=600
		)
		target.session = sessionmaker(bind=target.engine)()

	@hybridmethod
	def query(self, *args, **kwargs):
		return self.session.query(*args, **kwargs)

	@hybridmethod
	def execute(self, *args, **kwargs):
		return self.engine.execute(*args, **kwargs)
